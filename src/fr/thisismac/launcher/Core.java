package fr.thisismac.launcher;

import fr.theshark34.openauth.AuthPoints;
import fr.theshark34.openauth.AuthenticationException;
import fr.theshark34.openauth.Authenticator;
import fr.theshark34.openauth.model.AuthAgent;
import fr.theshark34.openauth.model.response.AuthResponse;
import fr.thisismac.launcher.elements.*;
import fr.thisismac.launcher.java.JavaProcessLauncher;
import fr.thisismac.launcher.utils.ConfigUtils;
import fr.thisismac.launcher.utils.DirUtils;
import fr.thisismac.launcher.utils.DirUtils.OS;
import fr.thisismac.launcher.utils.EncryptUtils;
import jdk.nashorn.internal.objects.annotations.Getter;
import sun.rmi.runtime.Log;
import sun.security.util.Debug;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

public class Core extends JFrame implements KeyListener, MouseListener {

	/* Initialisation de l'instance */
	private static Core instance;

	public static Core get() {
		return instance;
	}

	/* Initialisation de la font */
	public Font font = Font.getFont("Arial");

	/*   Initialisation des composents graphique*/
	final OptionsFrame optionsFrame = new OptionsFrame();
	CustomTextField user = new CustomTextField(75, 230, "/field.png");
	CustomPasswordField password = new CustomPasswordField(75, 280, "/field.png");
	CustomButton quit = new CustomButton(860, 7, "/close_normal.png", "/close_hover.png");
	CustomButton reduce = new CustomButton(820, 7, "/reduce_normal.png", "/reduce_hover.png");
	CustomButton options = new CustomButton(780, 7, "/options_normal.png", "/options_hover.png");
	CustomBar bar = new CustomBar(350, 483, "/progress_full.png");
	CustomImage play = new CustomImage(100, 350, "/connexion.png");
	CustomImage forgot = new CustomImage(200, 480, "/forgot_pwd.png");
	CustomImage register = new CustomImage(10, 480, "/register.png");
	
	/* Initialisation de l'auth et de l'updater */
	final AuthPoints points = new AuthPoints("authenticate", "refresh", "validate", "signout", "invalidate");
	final Authenticator authenticator = new Authenticator(Authenticator.MOJANG_AUTH_URL, points);
	AuthResponse response;
	
	// Variable random
	private Point initialClick;
	boolean debug;

	
	public Core(boolean debug) {
		instance = this;
		this.debug = debug;
		optionsFrame.setFocus(false);
		
		setSize(ConfigUtils.SIZE_X, ConfigUtils.SIZE_Y);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
		setLayout(null);
		setUndecorated(true);
		addKeyListener(this);
		setFocusTraversalKeysEnabled(false);
		setFocusable(true);
		
		 try { 
			 setIconImage(ImageIO.read(Core.class.getResource("/favicon.png")));
		 } catch (Exception e) {
			 e.printStackTrace();
		 }
		
		addMouseListener(new MouseAdapter() {
			public void mousePressed(MouseEvent e) {
				initialClick = e.getPoint();
				getComponentAt(initialClick);
			}
		});
		addMouseMotionListener(new MouseMotionAdapter() {
			@Override
			public void mouseDragged(MouseEvent e) {
				int thisX = getLocation().x;
				int thisY = getLocation().y;

				int xMoved = (thisX + e.getX()) - (thisX + initialClick.x);
				int yMoved = (thisY + e.getY()) - (thisY + initialClick.y);

				int X = thisX + xMoved;
				int Y = thisY + yMoved;
				setLocation(X, Y);
			}
		});
		
		quit.setOpaque(false);
		reduce.setOpaque(false);
		options.setOpaque(false);
		
		user.addKeyListener(this);
		password.addKeyListener(this);
		
		reduce.addMouseListener(this);
		options.addMouseListener(this);
		quit.addMouseListener(this);
		play.addMouseListener(this);
		forgot.addMouseListener(this);
		register.addMouseListener(this);
		
		bar.setOpaque(false);
		
		user.setFont(font);
		password.setFont(font);
		bar.setFont(font);
		
		
		CustomBackground bg = new CustomBackground();
		bg.setBounds(0, 0, ConfigUtils.SIZE_X, ConfigUtils.SIZE_Y);

		add(user);
		add(password);
		add(quit);
		add(reduce);
		add(play);
		add(options);
		add(bar);
		add(forgot);
		add(register);
		add(bg);

		EncryptUtils.readLastLogin(user, password);
		setVisible(true);
	}

	public void play() {
			System.out.println("Calling AUTH for user : " + user.getText());
			String reason = null;
			try {
				response = authenticator.authenticate(AuthAgent.MINECRAFT, user.getText(), password.getText(), null);
			} catch (AuthenticationException e) {
				reason = e.getErrorModel().getErrorMessage();
			}
						
			if (response == null) {
				JOptionPane.showMessageDialog(new Frame(), reason, "Probl�me d'authentification !", 0);
				password.setText("");
			} 
			else {
				if(optionsFrame.isRememberChecked()) {
					EncryptUtils.saveLastLogin(user.getText(), password.getText());
				}
				else if(EncryptUtils.lastlogin.exists()){
					EncryptUtils.lastlogin.delete();
				}
				user.setEditable(false);
				password.setEditable(false);
				//new UpdaterThread().run(); //start
				launchGame();
			}
	}
	
	public void launchGame(){
		File[] libs = new File(DirUtils.getWorkingDirectory(), "bin" + File.separator + "libs").listFiles();
		StringBuilder strBuilder = new StringBuilder();
        System.out.println(DirUtils.getPath() + ":" + DirUtils.getWorkingDirectory());
		for(File file : libs) {
			strBuilder.append(file.getAbsolutePath() + (DirUtils.getPlatform() == OS.windows ? ";" : ":"));
		}
		
		File tmp = new File(DirUtils.getPath() + File.separator + "assets", "skins");
		if (!tmp.exists())
			tmp.mkdir();
		JavaProcessLauncher processLauncher = new JavaProcessLauncher(null, 
				new String[] {
				  "-Xms512m" , 
				  "-Xmx" + Core.get().getOptionsFrame().getSelectedRAM() ,
				  //"-Dlog4j.configurationFile=" + DirUtils.getPath() + "" + File.separator + "log4j2.xml",
				  "-Djava.library.path=" + DirUtils.getPath() + "" + File.separator + "bin" + File.separator + "natives" + File.separator,
				  "-cp", strBuilder.toString() + DirUtils.getPath() + "" + File.separator + "bin" + File.separator + "minecraft.jar",
				  "net.minecraft.client.main.Main",
				  "--username", Core.get().getResponse().getSelectedProfile().getName(),
				  "--version", "1.8",
				  "--gameDir", DirUtils.getWorkingDirectory().getAbsolutePath(),
				  "--assetIndex", "1.8.8",
				  "--assetsDir", DirUtils.getPath() + File.separator + "assets",
				  "--uuid", Core.get().getResponse().getSelectedProfile().getId(),
				  "--userProperties", "{}",
				  "--accessToken", Core.get().getResponse().getAccessToken()});
		processLauncher.directory(DirUtils.getWorkingDirectory());
		try {
			processLauncher.start();
			
			//if (!debug)
			// System.exit(0);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private AuthResponse getResponse() {
		return response;
	}

	private OptionsFrame getOptionsFrame() {
		return optionsFrame;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		if (e.getKeyCode() == KeyEvent.VK_ENTER && user.getText().length() > 0 && !bar.isDownload()) {
			play();
		}
	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if(e.getSource() == play && user.getText().length() > 0 && !bar.isDownload()) {
			play();
		} 
		else if (e.getSource() == quit) {
			System.exit(0);
		}
		else if (e.getSource() == reduce) {
			setState(JFrame.ICONIFIED);
		} 
		else if(e.getSource() == options) {
			optionsFrame.setFocus(true);
		}
		else if(e.getSource() == forgot) {
			try {
				Desktop.getDesktop().browse(new URI("http://obsifight.fr/connexion"));
			} catch (IOException e1) {} catch (URISyntaxException e1) {}
		}
		else if(e.getSource() == register) {
			try {
				Desktop.getDesktop().browse(new URI("http://obsifight.fr/obsifight/user/signup"));
			} catch (IOException e1) {} catch (URISyntaxException e1) {}
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {}

	@Override
	public void keyTyped(KeyEvent arg0) {}

	@Override
	public void mousePressed(MouseEvent e) {}

	@Override
	public void mouseReleased(MouseEvent e) {}

	@Override
	public void mouseEntered(MouseEvent e) {
		if(e.getSource() instanceof CustomImage) {
			setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
		}
	}

	@Override
	public void mouseExited(MouseEvent e) {
		if(e.getSource() instanceof CustomImage) {
			setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
		}
	}

	public CustomBar getBar() {
		return bar;
	}
}
